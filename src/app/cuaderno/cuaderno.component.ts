import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cuaderno',
  templateUrl: './cuaderno.component.html',
  styleUrls: ['./cuaderno.component.scss']
})
export class CuadernoComponent implements OnInit {
  
  @Input() texto: string;
  constructor() { }

  ngOnInit() {
  }

}
